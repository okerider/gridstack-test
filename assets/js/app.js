
// form builder function
var formBuilder = function () {
  // variables
  var formBuilderClass = '.grid-stack'
  var formBuilder

  // init gridstack
  $(formBuilderClass).gridstack({
    cellHeight: 80,
    verticalMargin: 10,
    width: 12
  })
  formBuilder = $(formBuilderClass).data('gridstack')

  // --------------------
  // ----- function
  // add element to box
  var addElement = function (elContent) {
    console.log('Adding new widget')
    formBuilder.addWidget(
      $('<div class="grid-stack-item ui-draggable ui-resizeable"><div class="grid-stack-item-content">' + elContent + '</div></div>'),
      0, 0, 3, 2, true
    )
  }

  // import form builder
  var importFormBuilder = function (formContent) {
    console.log('Importing form builder')
    formBuilder.removeAll()

    formContent = JSON.parse(formContent)
    let items = GridStackUI.Utils.sort(formContent)

    _.forEach(items, function (node) {
      addElement(node.content, node.x, node.y, node.width, node.height, false)
    })
  }

  // export form builder
  var exportFormBuilder = function () {
    console.log('Exporting form builder')
    let formContent = $(formBuilderClass).children()
    let formContentSerialized = []

    _.forEach(formContent, function (widget) {
      let node = {}
      node.x = $(widget).data('gs-x')
      node.y = $(widget).data('gs-y')
      node.width = $(widget).data('gs-width')
      node.height = $(widget).data('gs-height')
      node.content = $(widget).children('.grid-stack-item-content').html()
      formContentSerialized.push(node)
    })

    return JSON.stringify(formContentSerialized)
  }

  // --------------------
  // ----- element interaction
  // drag and drop function
  $('.form-builder-element').draggable({ revert: true })
  $(formBuilderClass).droppable({
    drop: function (event, ui) {
      if ($(ui.draggable[0]).hasClass('form-builder-element')) {
        let elContent = $(ui.draggable[0]).find('.element-content').html()
        addElement(elContent)
      }
    }
  })

  $('.form-builder-export').click(function () {
    $('.form-builder-serialized').val(exportFormBuilder())
  })

  $('.form-builder-import').click(function () {
    formBuilder.removeAll()
    let formBuilderContent = $('.form-builder-serialized').val()
    importFormBuilder(formBuilderContent)
  })

  $('.form-builder-clear').click(function () {
    formBuilder.removeAll()
  })
}// formBuilder

$(document).ready(function () {
  formBuilder()
})
